# lib_twitter #
This library is intended to make it easier to incorporate twitter into PHP applications. It handles the OAuth authentication and is OOP-free.

### Requirements ###
* PHP 5
* A twitter account
* A registered twitter application


### Installation ###
** Setting Up A Twitter Application ** 

* Go to https://apps.twitter.com
* Log in using your usual details
* Hit `Create New App`
* Enter the details requested
* Ensure the permissions are set to read/write
* Generate API keys - if you generated them before changing permissions then regenerate the access token
* Edit lib_twitter.php and change the four twitter variables at the top to those you just generated, then save

** Install lib_twitter **

Simply put lib_twitter somewhere in the directory structure and include it:

    include 'lib_twitter.php'`



### Usage ###
There is a guide on using the various twitter resources at https://dev.twitter.com/docs/api/1.
$returned_json = TwitterAPI("twitter_resource.json", "method", "parameter_array);


```
#!php

// get the most recent 4 tweets posted by @my_username
$tweets = TwitterAPI("user_timeline.json", "GET", array("screen_name" => "my_username", "count" => "4"));

// post a new tweet
$return = TwitterAPI("update.json", "POST", array("status" => "Insert the text of the message you want to send"));
```

The return string contains the json output sent back by twitter. It can be decoded using `json_decode()`.