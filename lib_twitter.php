<?php
// the top 4 keys come from Twitter and are application-specific
// $request_query is an array of the parameters needed by twitter

// eg $raw_json = TwitterAPI("user_timeline.json", "GET", array("screen_name" => "my_screenname", "count" => "1"));


function TwitterAPI($resource, $method, $request_query)
{
   // these values come from the application setup page on twitter dev account
   // the application must have read and write priviledges - if change from read-only then need to generate new access keys
   $consumer_key = "aaaaaaaaaaaaaaaaaaaaaaaaaa";
   $consumer_secret = "bbbbbbbbbbbbbbbbbbbbbbbbbbb";
   $access_token = "cccccccccccccccccccccccccc";
   $access_token_secret = "ddddddddddddddddddddddddddddd";

   $URL_base = "https://api.twitter.com/1.1/statuses/";
   $twitter_resource = $URL_base . $resource;
   $method = strtoupper($method);		// just in case

   // generate a random text string as nonce and set timestamp to seconds since epoch
   $oauth_nonce = str_replace(array("+", "=", "#"), "", base64_encode(time() . mt_rand()));
   $oauth_timestamp = time();


   // now some of these need to be used to create signature and need to be in alphabetical order
   // therefore we'll use an array so we can sort
   $oauth_parameters = array(
	"oauth_consumer_key" => $consumer_key,
	"oauth_nonce" => $oauth_nonce,
	"oauth_signature_method" => "HMAC-SHA1",
	"oauth_timestamp" => $oauth_timestamp,
	"oauth_token" => $access_token,
	"oauth_version" => "1.0"
   );



   $oauth_parameters = array_merge($oauth_parameters, $request_query);
   ksort($oauth_parameters);


   // from parameters generate a parameter string: alphabetical order, percent-encoded: key=value&key2=value2
   $parameter_string = http_build_query($oauth_parameters, '' ,'&' , PHP_QUERY_RFC3986);


   // generate signature base string using method, url and parameter string
   $base_string = $method . "&" . rawurlencode($twitter_resource) . "&" . rawurlencode($parameter_string);


   // generate key
   $key = rawurlencode($consumer_secret) . "&" . rawurlencode($access_token_secret);


   // and now make the signature from the signature base string and the key
   $signature = rawurlencode(base64_encode(hash_hmac('sha1', $base_string, $key, 1)));


   // make the query string for get requests
   ksort($request_query);
   $query_string = http_build_query($request_query);

/*   foreach ($request_query as $key => $value)
   {
      $query_string = $query_string . $key . "=" . $value . "&";
   }
   $query_string = rtrim($query_string, "&");
*/


   // need to execute query and return json
   $headers = array(
      "http" => array(
         "method" => $method,
         "header"  => "Authorization: OAuth oauth_consumer_key=\"" . $consumer_key. "\", oauth_nonce=\"" . $oauth_nonce . "\", oauth_signature=\"" . $signature . "\",oauth_signature_method=\"HMAC-SHA1\", oauth_timestamp=\"" . $oauth_timestamp . "\", oauth_token=\"" . $access_token . "\", oauth_version=\"1.0\""
      )
   );

   if (strcmp($method, "POST") === 0)
   {
      $headers["http"]["header"] = "Content-Type: application/x-www-form-urlencoded\r\n" . $headers["http"]["header"];
      $headers["http"]["content"] = $query_string;
      $url = $twitter_resource;
   } else {
      $url = $twitter_resource . "?" . $query_string;
   }


   $context = stream_context_create($headers);

   $raw_twitterresult = file_get_contents($url, false, $context);	// this can be processed using json_decode($raw_twitterresult, true)
   return $raw_twitterresult;
}


?>
