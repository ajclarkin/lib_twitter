<?php
include("lib_twitter.php");

// test GET

$request_query = array(
	"screen_name" => "my_username",
	"count" => "1"
);

$result = TwitterAPI("user_timeline.json", "GET", $request_query);
$result = json_decode($result, true);
foreach ($result as $tweet)
{
   printf("%s", $tweet["text"]);
}

// test POST
/*
$request_query = array(
        "status" => "Here, finally is a test message from my php library. #test",
);

TwitterAPI("update.json", "POST", $request_query);
// alternative would be TwitterAPI("update.json", "POST", array("status" => "Here is a test message"));
*/
?>
